FROM python:3.9-slim

# install PySide dependencies
RUN apt update -yqq
RUN apt install -yqq libglib2.0-dev libgl-dev libegl-dev libxkbcommon-dev libdbus-1-dev

# install poetry
RUN pip install poetry
